module gitlab.com/brancengregory/tictactoe

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	gitlab.com/brancengregory/go-jsonconfig v0.0.0-20190709025614-47f9e2e62977
	gitlab.com/brancengregory/go-server v0.0.0-20190709040313-c72c1da43f56
	gonum.org/v1/gonum v0.0.0-20190704103327-70ddf0df3d53
)
