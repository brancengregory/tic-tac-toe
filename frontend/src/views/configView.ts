import { LitElement, property, customElement, html } from 'lit-element';
import * as helpers from '../helpers';


@customElement('config-view')
export class ConfigView extends LitElement {    
    
    configForm = {
        numPlayers: true,
        playerOneName: '',
        numRounds: 3
    }
    
    render() {
        return html`
            <div id="configForm">
                <p># of Players:</p>
                <select name="numPlayers">
                    <option value=1 ?selected=${this.configForm.numPlayers}>1</option>
                    <option value=2 ?selected=${!this.configForm.numPlayers}>2</option>
                </select>
                <p>Player 1 Name:</p>
                <input type="text" name="playerOneName" value=${this.configForm.playerOneName}>
                <p># of Rounds:</p>
                <input type="range" min=1 max=10 value=${this.configForm.numRounds} name="numRounds">
                <button @click=${this.configSubmitHandler} name="submit">Submit</button>
            </div>
        `;
    };

    encodeConfigForm() {
        return JSON.stringify(this.configForm)
    }

    configSubmitHandler(e: Event) {
        e.preventDefault();
        helpers.makeFetchPOST('/game/config', this.encodeConfigForm())
    };
};