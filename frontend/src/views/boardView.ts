import { LitElement, property, customElement, html } from 'lit-element';
import { repeat } from 'lit-html/directives/repeat';


@customElement('board-view')
export class BoardView extends LitElement {
    
    boardCellView = new BoardCell
    
    render() {

/*  Located in return below!!!!!
             ${repeat(this.cells, (cell) => cell.id, (cell) => html`
                <div class="boardCell" .symbol=${cell.symbol}>${cell.id}</div>
            `)} */
        return html`
            ${this.boardCellView}
        `;
    }
};

@customElement('board-cell')
export class BoardCell extends LitElement {
    cell = {
        id: '1',
        played: true,
        symbol: 'x'
    }


    render() {
        if(this.cell.played == true) {
            return html`
                ${this.cell.symbol == 'o' ?
                    html`
                        <div .symbol=${this.cell.symbol}>
                            <svg width="100%" height="100%">
                                <circle cx="50%" cy="50%" r="30%" stroke="black" fill="transparent" stroke-width="5"/>
                            </svg>
                        </div>
                    ` :
                    html`
                        <div .symbol=${this.cell.symbol}>
                            <svg width="100%" height="100%">
                                <line x1="20%" y1="20%" x2="80%" y2="80%" stroke="black" fill="transparent" stroke-width="5"/>
                                <line x1="80%" y1="20%" x2="20%" y2="80%" stroke="black" fill="transparent" stroke-width="5"/>
                            </svg>
                        </div>
                    `
                }
            `;
        } else {
            return html`
                <div>${this.cell.id}</div>
            `;
        }

    }
    
}

const oTemplate = html`
    <svg width="100%" height="100%">
        <circle cx="50%" cy="50%" r="30%" stroke="black" fill="transparent" stroke-width="5"/>
    </svg>`

const xTemplate = html`
    <svg width="100%" height="100%">
        <line x1="20%" y1="20%" x2="80%" y2="80%" stroke="black" fill="transparent" stroke-width="5"/>
        <line x1="80%" y1="20%" x2="20%" y2="80%" stroke="black" fill="transparent" stroke-width="5"/>
    <svg>`

/*     { id: 1, played: false , symbol: 'x'},
    { id: 2, played: false , symbol: 'o'},
    { id: 3, played: false , symbol: 'x'},
    { id: 4, played: false , symbol: 'o'},
    { id: 5, played: false , symbol: 'x'},
    { id: 6, played: false , symbol: 'x'},
    { id: 7, played: false , symbol: 'o'},
    { id: 8, played: false , symbol: 'x'},
    { id: 9, played: false , symbol: 'o'}, */