import { LitElement, property, customElement, html } from 'lit-element';
import * as config from './views/configView'
import * as board from './views/boardView'


//Main tictactoe element
@customElement('tic-tac-toe')
export class TicTacToe extends LitElement {    
    configView = new config.ConfigView
    boardView = new board.BoardView
    
    render() {
        return html`
            ${titleTemplate}
            ${this.configView}
            ${this.boardView}
        `;
    }
}

const titleTemplate = html`<p>Tic-Tac-Toe</p>`;