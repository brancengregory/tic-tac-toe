//Helper Funcs
export function formToJSON(form: HTMLFormElement): string {    
    let data: Object = {};

    for (var Control of form.elements) {
        console.log(Control)
    }

    return JSON.stringify(data);
};

export function makeFetchGET(url: string) {
    fetch(url, {
        method: 'GET',
        mode: "same-origin",
        credentials: "same-origin"
    })
    .then(function(response) {
        if (!response.ok) {
            alert("Response was bad...");
        }
    })
    .catch(function(error) {
        console.log('Request Failed: ', error)
    });
}

export function makeFetchPOST(url: string, data: string) {
    fetch(url, {
        method: 'POST',
        mode: "same-origin",
        credentials: "same-origin",
        headers: {'Content-Type': 'application/json'},
        body: data
    })
    .then(function(response) {
        if (!response.ok) {
            alert("Response was bad...");
        }
    })
    .catch(function(error) {
        console.log('Request Failed: ', error)
    });
}