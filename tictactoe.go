package main

import (
	"os"

	jsonconfig "gitlab.com/brancengregory/go-jsonconfig"
	server "gitlab.com/brancengregory/go-server"
	"gitlab.com/brancengregory/tictactoe/src/route"
)

var serverconfig = &server.Config{}

func main() {
	jsonconfig.Load("config"+string(os.PathSeparator)+"config.json", serverconfig)
	server.Run(route.Load(), serverconfig.Server)
}
