package model

import "fmt"

//Config holds game configuration options
type Config struct {
	NumPlayers int
	NumRounds  int
}

//Game is the base struct
type Game struct {
	Players   []Player
	Rounds    []Round
	Completed bool
	Winner    Player
	Config    Config
}

//Configure configures the game
func (g Game) Configure() {

}

//Start starts the game
func (g Game) Start() {

}

//End ends the game
func (g Game) End() {

}

//Round hols all board states in a single round
type Round struct {
	Number    int
	Boards    []Board
	Completed bool
	Winner    Player
}

//Start starts a round
func (r Round) Start() {

}

//End completes the next round
func (r Round) End() {

}

//Turn holds a player move and the turn number
type Turn struct {
	Number int
	Player Player
	Cell   Cell
}

//Start runs a turn
func (t Turn) Start() {

}

//End completes a turn
func (t Turn) End(b Board) {
	b.WinCheck()
}

//Player will represent a player
type Player struct {
	Name   string
	Number int
	/* 	wins int
	   	losses int
	   	online bool */
}

//Move completes a move for a player
func (p Player) Move() Cell {

	return (Cell{})
}

//Position represents one cell of the board
type Position struct {
	Number int
}

//Cell represents a single cell state
type Cell struct {
	Position Position
	Played   bool
	Player   Player
}

//Update updates a single cell
func (c Cell) Update() Cell {

	return (Cell{})
}

//Board represents a single board state
type Board struct {
	Cells [9]Cell
}

//WinCheck checks to see if win conditions are met
func (b Board) WinCheck() bool {
	var v [9]float64
	for i, x := range b.Cells {
		if x.Played && x.Player.Number == 1 {
			v[i] = float64(1)
		}
		v[i] = 0
	}

	winners := getWinningBoards()

	for _, x := range winners {
		if x == v {
			return true
		}
	}

	return false
}

//Render renders a board state
func (b Board) Render() {
	for i, x := range b.Cells {
		fmt.Printf("Cell %v contains %v\n", i+1, x)
	}
}

func getWinningBoards() [8][9]float64 {
	var winners [8][9]float64
	winners[0] = [...]float64{1, 1, 1, 0, 0, 0, 0, 0, 0}
	winners[1] = [...]float64{0, 0, 0, 1, 1, 1, 0, 0, 0}
	winners[2] = [...]float64{0, 0, 0, 0, 0, 0, 1, 1, 1}
	winners[3] = [...]float64{1, 0, 0, 1, 0, 0, 1, 0, 0}
	winners[4] = [...]float64{0, 1, 0, 0, 1, 0, 0, 1, 0}
	winners[5] = [...]float64{0, 0, 1, 0, 0, 1, 0, 0, 1}
	winners[6] = [...]float64{1, 0, 0, 0, 1, 0, 0, 0, 1}
	winners[7] = [...]float64{0, 0, 1, 0, 1, 0, 1, 0, 0}
	return winners
}
