package route

import (
	"gitlab.com/brancengregory/tictactoe/src/controller"

	"net/http"

	"github.com/gorilla/mux"
)

//Load returns the routes
func Load() http.Handler {
	return routes()
}

func routes() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/", controller.IndexGET).Methods("GET")
	r.HandleFunc("/game/config", controller.ConfigPOST).Methods("POST")
	r.PathPrefix("/frontend/static/").Handler(http.StripPrefix("/frontend/static/", http.FileServer(http.Dir("./frontend/static/"))))

	return r
}
