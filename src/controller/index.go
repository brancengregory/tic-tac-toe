package controller

import (
	"net/http"
)

//IndexGET handles req to index
func IndexGET(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./frontend/static/index.html")
}
