package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/brancengregory/tictactoe/src/model"
)

//ConfigPOST handles req to index
func ConfigPOST(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(body)

	var g model.Game
	err = json.Unmarshal(body, &g)
	if err != nil {
		panic(err)
	}
}
